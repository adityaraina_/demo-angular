import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockGetComponent } from './stock-get.component';

describe('StockGetComponent', () => {
  let component: StockGetComponent;
  let fixture: ComponentFixture<StockGetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockGetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockGetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
