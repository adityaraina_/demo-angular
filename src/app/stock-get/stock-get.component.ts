import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";


@Component({
  selector: 'app-stock-get',
  templateUrl: './stock-get.component.html',
  styleUrls: ['./stock-get.component.css']
})
export class StockGetComponent implements OnInit {

  Stocks: any = [];
  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadStocks()
  }

  loadStocks() {
    return this.restApi.getAllStocks().subscribe((data: {}) => {
        this.Stocks = data;
    })
  }

}
